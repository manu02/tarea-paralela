#ifndef NOTAS_H
#define NOTAS_H
#include <fstream>

int validatedInput(int min, int max);
class notas
{
    public:
        notas();
        virtual ~notas();
        float promedio();
        float suma();
        int cantidad();
        float desv();
        void moda();
        void mediana();

    protected:

    private:
       std::ifstream archivo;
};

#endif // NOTAS_H
