#include <iostream>
#include <notas.h>
using namespace std;

int main()
{
    notas x;
    int n=-1;
    while (n!=7)
    {
        cout << "1) Suma total" << endl;
        cout << "2) Cantidad total de elementos" << endl;
        cout << "3) Media" << endl;
        cout << "4) Mediana" << endl;
        cout << "5) Moda" <<endl;
        cout << "6)Desviación Estándar" <<endl;
        cout << "7)Salir.\n";

        cout << "Ingrese opción: ";
        n = validatedInput(1,7);

        if (n==1)
        {
            cout<<"La suma total es : "<<x.suma()<<endl;
        }

        if (n==2)
        {
            cout <<"La cantidad total de elementos es : "<<x.cantidad()<<endl;
        }

        if (n==3)
        {
            cout<<"La Media es : "<<x.promedio()<<endl;
        }

        if (n==4)
        {
            cout<<"La Mediana es: ";
            x.mediana();
            cout<<endl;
        }

        if (n==5)
        {
            cout<<"La Moda es : ";
            x.moda();
            cout<<endl;
        }

        if (n==6)
        {
            cout<<"La Desviación Estándar es : "<<x.desv()<<endl;
        }

        cout<<endl;

    }
    return 0;
}
