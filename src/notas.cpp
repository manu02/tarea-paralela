#include "notas.h"
#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <fstream>
#include <cmath>
#include <vector>
#include <algorithm>

using namespace std;

int validatedInput(int min = 1, int max = 5)
{
    while(true)
    {
        string s;
        getline(cin,s);
        char *endp = 0;
        int ret = strtol(s.c_str(),&endp,10);
        if(endp!=s.c_str() && !*endp && ret >= min && ret <= max)
            return ret;
        cout << "\nEntrada inv�lida, pruebe otra vez: ";
    }
}


notas::notas()
{
    archivo.open("numeros.txt",ios::in);
    if(archivo.fail())
    {
        cout<<"Fallo al intentar abrir .txt"<<endl;
        exit(1);
    }
}

notas::~notas()
{
    archivo.close();
    //dtor
}

float notas::promedio()
{
    float suma=0;
    float numero;
    float contador=0;
    string line;
    while(getline(archivo,line))
    {

        replace(line.begin(), line.end(), ',', '.');
        numero = atof(line.c_str());
        suma=suma+numero;


        contador++;

    }
    float promedio=(suma/contador);
    archivo.clear();
    archivo.seekg (0, ios::beg);
    return promedio;

}

float notas::suma()
{
    float suma=0;
    float numero;
    string line;
    while(getline(archivo,line))
    {
        replace(line.begin(), line.end(), ',', '.');
        numero = atof(line.c_str());
        suma=suma+numero;

    }

    archivo.clear();
    archivo.seekg (0, ios::beg);
    return suma;

}

int notas::cantidad()
{


    int contador=0;
    string line;
    while(getline(archivo,line))
    {



        contador++;

    }
    archivo.clear();
    archivo.seekg (0, ios::beg);
    return contador;
}


float notas::desv()
{
    notas x;
    string line;
    float numero;
    float var=0;
    int cantidad=x.cantidad();
    float promedio=x.promedio();
    for (int i=0; i<cantidad; i++)
    {
        getline(archivo,line);
        replace(line.begin(), line.end(), ',', '.');
        numero = atof(line.c_str());
        var += pow(numero - promedio, 2);
    }
    archivo.clear();
    archivo.seekg (0, ios::beg);
    return sqrt(var/cantidad);
}


void notas::moda()
{
    notas x;
    vector <float> sumanotas;
    string line;
    float numero;
    int cantidad=x.cantidad();
    int c=0;
    int maximo=0;
    float valor;
    float valoraux;
    while(getline(archivo,line))
    {
        replace(line.begin(), line.end(), ',', '.');
        numero = atof(line.c_str());
        sumanotas.push_back(numero);
    }
    for (int i=0; i<cantidad; i++)
    {
        c=0;
        valor=sumanotas[i];
        for (int j=0; j<cantidad; j++)
        {
            if (valor==sumanotas[j])
            {
                c++;
                if (maximo<c)
                {
                    valoraux=valor;
                    maximo=c;
                }
            }

        }

    }
    archivo.clear();
    archivo.seekg (0, ios::beg);
    cout<<valoraux;

}

void notas::mediana()
{
    notas x;
    vector <float> sumanotas;
    string line;
    float numero;
    float mediana;
    int cantidad=x.cantidad();
    while(getline(archivo,line))
    {
        replace(line.begin(), line.end(), ',', '.');
        numero = atof(line.c_str());
        sumanotas.push_back(numero);
    }
    sort(sumanotas.begin(), sumanotas.end());
    if (cantidad%2!=0)
    {
        mediana=sumanotas[(cantidad/2)];
    }
    else
    {
        mediana=(sumanotas[(cantidad/2)]+sumanotas[(cantidad/2)-1])/2;
    }
    archivo.clear();
    archivo.seekg (0, ios::beg);
    cout<<mediana;
}
